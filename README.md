# Elixway

### _A gateway built with Elixir_
Elixway is a small project that tries to work as a gateway between clients and different services.

[![Generic badge](https://img.shields.io/badge/Elixir-1.12-green.svg)](https://shields.io/) [![Open Source Love png1](https://badges.frapsoft.com/os/v1/open-source.png?v=103)](https://github.com/ellerbrock/open-source-badges/) ![Maintaner](https://img.shields.io/badge/maintainer-gpopides-blue)

## Questions

> Why?
- For fun.  Gateways are applications that need to handle communication between multiple services spread across the network.
Erlang/OTP was created for such applications.
Elixir makes it a bit more pleasant and easier to work with OTP.

> Should i use it in production?
- No.  This is a toy project and for learning purposes.

> Why should i use it?
- Well if you try to learn about microservices, having multiple services on localhost and on different ports leads in using different endpoints in your client.  This app aims to fix that so you can work with 1 endpoint.

> Is it the best project for it's use case?
- No



## How does it work
When Elixway starts, it looks for a file named **endpoints.json** which contains the needed data and that looks like this:
```json
{
	"services": {
		"api": {
			"endpoints": ["login", "register"],
			"parents": [],
			"endpoint": "http://localhost:8081"
		}
	},
	"root": {
		"endpoint": "http://localhost:8080"
	}
}
```
Reading the file, Elixway builds a tree with endpoints that uses to look up when receiving a request.

### endpoints.json structure
#### root: 
The root endpoint, usually not needed
#### services: 
An object which has keys indicating nodes below root and values which are used to create sub nodes in the tree.
-- endpoints: used to create subnodes of the node
-- parents: a list of strings that indicate that are used to create parent nodes of the specified node
-- endpoint: the endpoint that the services can be accessed at
For examnple
```json
{
    "e": {
        "parents": ["a", "b"],
        "endpoints": ["c", "d"],
        "endpoint": "X"
    }
}
```
Will create a node with this path:
> http://X/a/b/c/d/e

### Running the app
`mix run --no-halt`
### Tests
`mix test`
