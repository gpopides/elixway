defmodule Elixway.RouteHandler do
  import Plug.Conn
  require Logger
  alias Elixway.EndpointPathTree

  def handle(%Plug.Conn{path_info: path_info} = conn) do
    url_path = Enum.join(path_info, "/")
    do_handle(conn, is_auth_path?(url_path))
  end

  @spec do_handle(conn :: Plug.Conn.t(), auth_tuple_path_check :: {String.t(), boolean()}) ::
          Plug.Conn.t()
  defp do_handle(conn, {path, false}) do
    case EndpointPathTree.find(Elixway.TreeManager.get_tree(), path) do
      {:ok, node} ->
        endpoint = Map.get(node, :endpoint)
        forward_request(conn, endpoint)

        conn
        |> log_url_search_result(true)
        |> send_resp(200, Jason.encode!(%{success: endpoint}))

      {:error, :not_found} ->
        conn
        |> log_url_search_result(false)
        |> send_resp(404, Jason.encode!(%{error: "some error"}))
    end
  end

  defp do_handle(conn, {path, true}) do
    case EndpointPathTree.find(Elixway.TreeManager.get_tree(), path) do
      {:ok, node} ->
        conn
        |> log_url_search_result(true)
        |> send_resp(200, Map.get(node, :endpoint))

      {:error, :not_found} ->
        conn
        |> log_url_search_result(false)
        |> send_resp(404, "not_found")
    end
  end

  defp is_auth_path?(path), do: {path, MapSet.member?(get_auth_endpoints(), path)}

  @spec forward_request(conn :: %Plug.Conn{}, endpoint :: String.t()) :: any()
  defp forward_request(conn, endpoint) do
    %{
      req_headers: headers,
      query_string: query_params,
      request_path: path,
      method: method,
      body_params: body
    } = conn

    # path contains in the beginning the / character
    # which we we don't want to include since endpoint
    # variable already contains it in the it's end.
    sliced_path = String.slice(path, 1..-1)

    url =
      case query_params do
        "" -> "#{endpoint}/#{sliced_path}"
        _ -> "#{endpoint}/#{sliced_path}?#{query_params}"
      end

    case method do
      "GET" ->
        Finch.build(:get, url, headers) |> Finch.request(ElixwayFinch)

      "POST" ->
        Finch.build(:post, url, headers, Jason.encode!(body)) |> Finch.request(ElixwayFinch)
    end
  end

  defp get_auth_endpoints(), do: MapSet.new(["login", "register"])

  @spec log_url_search_result(conn :: %Plug.Conn{}, found :: boolean()) :: %Plug.Conn{}
  defp log_url_search_result(conn, found) do
    Logger.info("Got request for #{inspect(conn.path_info)}.  Found: #{found}")
    conn
  end
end
