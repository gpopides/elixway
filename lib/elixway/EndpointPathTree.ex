defmodule Elixway.EndpointPathTree do
  alias Elixway.EndpointPathNode
  require Logger

  @spec create_from_multiple_children(
          root :: map(),
          nodes_collection :: List.EndpointPathNode.t()
        ) :: any()
  def create_from_multiple_children(root, nodes_collection) do
    p = EndpointPathNode.new("/", Map.get(root, "endpoint"))

    Enum.reduce(nodes_collection, p, fn node, acc ->
      case find(acc, node.path) do
        {:error, :not_found} -> EndpointPathNode.add_child(acc, node)
        {:ok, parent_node} -> EndpointPathNode.add_child(parent_node, node)
      end
    end)
  end

  @spec find(root :: EndpointPathNode.t(), path_to_find :: String.t()) ::
          {:ok, any()} | {:error, :not_found}
  def find(%EndpointPathNode{} = root, path_to_find) do
    path_parts = get_path_parts(path_to_find)

    parts_have_different_height? = length(path_parts) > height(root)

    if parts_have_different_height? do
      {:error, :not_found}
    else
      do_find(root, path_parts)
      |> handle_find_result()
    end
  end

  def height(%EndpointPathNode{children: children}, current_height \\ 0) do
    num_of_keys = Map.keys(children) |> length

    case num_of_keys do
      0 when current_height == 0 ->
        1

      0 when current_height != 0 ->
        current_height + 1

      _ ->
        max_children_height =
          Map.values(children)
          |> Enum.map(fn child -> height(child, 0) end)
          |> Enum.max()

        max_children_height + 1
    end
  end

  @spec initialize(file_path :: String.t()) :: EndpointPathNode.t()
  def initialize(file_path) do
    File.read!(file_path)
    |> Jason.decode()
    |> log_initialization()
    |> create_tree()
  end

  defp create_tree({:error, _reason}), do: %{}

  defp create_tree({:ok, %{"services" => services, "root" => root}}) when is_map(services) do
    nodes =
      Enum.map(services, fn {k, v} -> %{k => v} end)
      |> Task.async_stream(Elixway.EndpointPathNode, :create_from_endpoints_json, [])
      |> Enum.map(fn {:ok, x} -> x end)

    create_from_multiple_children(root, nodes)
  end

  @spec do_find(node :: EndpointPathNode.t(), path_parts :: list(String.t())) ::
          {:ok, EndpointPathNode.t()} | {:error, :not_found}
  defp do_find(node, path_parts) do
    [head | tail] = path_parts

    if Enum.empty?(tail) do
      check_if_node_matches_path_part(node, head)
    else
      do_search_path(node.children, tail)
    end
  end

  @spec do_search_path(children :: map(), path_parts :: list(String.t())) ::
          {:ok, EndpointPathNode.t()} | {:error, :not_found}
  defp do_search_path(children, t) do
    [head | tail] = t

    lookup_key =
      if Enum.any?(Map.values(children), &EndpointPathNode.is_node_path_param?/1),
        do: Map.keys(children) |> List.first(),
        else: head

    case Map.get(children, lookup_key) do
      nil -> {:error, :not_found}
      matching_node when tail != [] -> do_search_path(matching_node.children, tail)
      matching_node -> {:ok, matching_node}
    end
  end

  @spec check_if_node_matches_path_part(node :: EndpointPathNode.t(), path_part :: String.t()) ::
          {:ok, EndpointPathNode.t()} | {:error, :not_found}
  defp check_if_node_matches_path_part(node, path_part) do
    if Map.get(node, :path) == path_part do
      {:ok, node}
    else
      {:error, :not_found}
    end
  end

  defp handle_find_result({:ok, node}), do: {:ok, node}
  defp handle_find_result(_), do: {:error, :not_found}

  defp get_path_parts(path) do
    # / is root, so we need to add it to the list because when spliting a string with /
    # we don't we lose 1 list entry 
    path_parts = String.split(path, "/") |> List.insert_at(0, "/")

    path_parts_without_leading_space = fn x ->
      [_ | tail] = x
      tail
    end

    first_path_part_length = List.first(path_parts) |> String.length()

    if first_path_part_length == 0,
      do: path_parts_without_leading_space.(path_parts),
      else: path_parts
  end

  defp log_initialization({:ok, tree}) do
    Logger.info("Initated tree: #{inspect(tree)}")
    {:ok, tree}
  end

  defp log_initialization({:error, reason}) do
    Logger.error("Could not initialize tree")
    {:error, reason}
  end
end
