defmodule Elixway.EndpointPathNode do
  @type t :: %__MODULE__{
          path: String.t(),
          endpoint: String.t(),
          children: map()
        }

  defstruct [:path, :endpoint, :children]

  alias Elixway.EndpointPathNode

  @spec new(path :: String.t(), endpoint: String.t()) :: t()
  def new(path, endpoint) do
    %Elixway.EndpointPathNode{
      path: path,
      children: Map.new(),
      endpoint: endpoint
    }
  end

  @spec add_child(parent :: EndpointPathNode.t(), child :: EndpointPathNode.t()) ::
          EndpointPathNode.t()
  def add_child(%EndpointPathNode{} = parent, %EndpointPathNode{} = child) do
    new_children =
      case node_path_has_subpaths?(child.path) do
        true ->
          create_children_subnodes_and_merge(parent, child)

        false ->
          Map.merge(parent.children, %{child.path => child})
      end

    Map.put(parent, :children, new_children)
  end

  @spec add_children(parent :: EndpointPathNode.t(), child :: list()) ::
          EndpointPathNode.t()
  def add_children(%EndpointPathNode{} = parent, children) when is_list(children) do
    List.foldl(children, parent, fn child, parent_acc -> add_child(parent_acc, child) end)
  end

  def create_children_from_path_parts(path_parts, endpoint) do
    [head | tail] = path_parts
    parent = new(head, endpoint)

    cond do
      length(tail) == 0 ->
        parent

      length(tail) > 0 ->
        subpath_nodes = create_children_from_path_parts(tail, endpoint)
        add_child(parent, subpath_nodes)
    end
  end

  @spec remove_child(parent :: EndpointPathNode.t(), child :: EndpointPathNode.t()) ::
          EndpointPathNode.t()
  def remove_child(%EndpointPathNode{} = parent, %EndpointPathNode{} = child) do
    new_children = Map.delete(parent.children, child.path)
    Map.put(parent, :children, new_children)
  end

  def create_from_endpoints_json(item) do
    [
      {
        path,
        %{"endpoint" => endpoint, "endpoints" => endpoints}
      }
    ] = Map.to_list(item)

    parent_node = EndpointPathNode.new(path, endpoint)

    add_children_from_children_list(parent_node, endpoints, endpoint)
  end

  defp add_children_from_children_list(parent_node, children, endpoint) do
    children = Enum.map(children, fn x -> EndpointPathNode.new(x, endpoint) end)
    EndpointPathNode.add_children(parent_node, children)
  end

  @spec is_node_path_param?(node :: EndpointPathNode.t()) :: boolean
  def is_node_path_param?(node), do: Regex.match?(~r/{.*}*/, node.path)

  defp node_path_has_subpaths?(path), do: length(String.split(path, "/")) > 1

  defp create_children_subnodes_and_merge(
         parent = %EndpointPathNode{},
         child = %EndpointPathNode{}
       ) do
    child_node_sub_paths = String.split(child.path, "/")
    current_child_path = List.first(child_node_sub_paths)

    child_sub_nodes = create_children_from_path_parts(child_node_sub_paths, child.endpoint)

    Map.merge(parent.children, %{current_child_path => child_sub_nodes}, fn _key, left, right ->
      merged_children = Map.merge(left.children, right.children)
      Map.put(left, :children, merged_children)
    end)
  end
end
