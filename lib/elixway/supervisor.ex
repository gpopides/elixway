defmodule Elixway.Supervisor do
  use Supervisor

  def start_link(opts) do
    Supervisor.start_link(__MODULE__, :ok, opts)
  end

  @impl true
  def init(:ok) do
    children = [
      {Elixway.TreeManager, name: Elixway.TreeManager},
      {Plug.Cowboy, scheme: :http, plug: Elixway.Router, port: 4040},
      {Finch, name: ElixwayFinch}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    Supervisor.init(children, strategy: :one_for_one)
  end
end
