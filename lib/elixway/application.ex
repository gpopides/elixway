defmodule Elixway.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  alias Elixway.EndpointPathTree

  use Application

  @impl true
  def start(_type, _args) do
    Elixway.Supervisor.start_link(name: Elixway.Supervisor)
  end
end
