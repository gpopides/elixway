defmodule Elixway.TreeManager do
  use GenServer
  require Logger

  alias Elixway.EndpointPathTree
  alias Elixway.EndpointPathNode

  @table_name :endpoints_tree

  @doc false
  def start_link(options) do
    GenServer.start_link(__MODULE__, :ok, options)
  end

  @impl true
  def init(:ok) do
    EndpointPathTree.initialize("endpoints.json")
    |> initialize_ets()
    {:ok, {}}
  end

  @spec get_tree() :: EndpointPathNode.t()
  def get_tree() do
    with {_, tree} <- :ets.lookup(@table_name, "tree") |> List.first() do
      tree
    end
  end

  @spec initialize_ets(tree :: EndpointPathNode.t()) :: boolean()
  defp initialize_ets(tree) do
    :ets.new(@table_name, [:protected, :named_table, read_concurrency: true])
    :ets.insert(@table_name, {"tree", tree})
  end

end
