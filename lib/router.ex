defmodule Elixway.Router do
  use Plug.Router

  plug(Plug.Parsers,
    parsers: [:json],
    json_decoder: Jason
  )

  plug(:match)
  plug(:dispatch)

  match _ do
    Elixway.RouteHandler.handle(conn)
  end
end
