defmodule ElixwayTest do
  use ExUnit.Case

  alias Elixway.EndpointPathNode

  @endpoint "http://localhost:8000"

  test "create new node" do
    assert %EndpointPathNode{path: "value", endpoint: @endpoint, children: %{}} ==
             EndpointPathNode.new("value", @endpoint)
  end

  test "add 1 child" do
    node = EndpointPathNode.new("value", @endpoint)
    child_node = EndpointPathNode.new("child", @endpoint)

    new_node = EndpointPathNode.add_child(node, child_node)

    assert new_node == %EndpointPathNode{
             path: "value",
             children: %{
               "child" => child_node
             },
             endpoint: @endpoint
           }
  end

  test "add 2 children" do
    node = EndpointPathNode.new("value", @endpoint)
    child_node = EndpointPathNode.new("child", @endpoint)
    child_node2 = EndpointPathNode.new("child2", @endpoint)

    new_node = EndpointPathNode.add_child(node, child_node)
    new_node = EndpointPathNode.add_child(new_node, child_node2)

    assert new_node == %EndpointPathNode{
             path: "value",
             children: %{
               "child" => child_node,
               "child2" => child_node2
             },
             endpoint: @endpoint
           }
  end

  test "remove child" do
    node = EndpointPathNode.new("value", @endpoint)
    child_node = EndpointPathNode.new("child", @endpoint)
    child_node2 = EndpointPathNode.new("child2", @endpoint)

    new_node = EndpointPathNode.add_child(node, child_node)
    new_node = EndpointPathNode.add_child(new_node, child_node2)

    new_node = EndpointPathNode.remove_child(new_node, child_node)

    assert new_node == %EndpointPathNode{
             path: "value",
             children: %{
               "child2" => child_node2
             },
             endpoint: @endpoint
           }
  end

  test "add child with it's own child node" do
    node = EndpointPathNode.new("value", @endpoint)
    child_node = EndpointPathNode.new("child", @endpoint)
    grandchild = EndpointPathNode.new("grandchild", @endpoint)

    child_with_granchild =
      child_node
      |> EndpointPathNode.add_child(grandchild)

    node = EndpointPathNode.add_child(node, child_with_granchild)

    assert %EndpointPathNode{
             path: "value",
             endpoint: @endpoint,
             children: %{
               "child" => %EndpointPathNode{
                 path: "child",
                 endpoint: @endpoint,
                 children: %{
                   "grandchild" => grandchild
                 }
               }
             }
           } == node
  end

  test "add children" do
    node = EndpointPathNode.new("value", @endpoint)

    children = [
      EndpointPathNode.new("child", @endpoint),
      EndpointPathNode.new("child2", @endpoint)
    ]

    assert %EndpointPathNode{
             path: "value",
             endpoint: @endpoint,
             children: %{
               "child" => %EndpointPathNode{
                 path: "child",
                 endpoint: @endpoint,
                 children: %{}
               },
               "child2" => %EndpointPathNode{
                 path: "child2",
                 endpoint: @endpoint,
                 children: %{}
               }
             }
           } == EndpointPathNode.add_children(node, children)
  end
end
