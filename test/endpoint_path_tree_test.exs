defmodule Elixway.EndpointPathTreeTest do
  use ExUnit.Case

  alias Elixway.EndpointPathTree
  alias Elixway.EndpointPathNode

  @endpoint "http://localhost:8000"

  test "EndpointPathTree.height/1 returns 1" do
    root = EndpointPathNode.new("api", @endpoint)
    assert 1 == EndpointPathTree.height(root)
  end

  test "EndpointPathTree.height/1 returns 3" do
    root =
      EndpointPathNode.new("api", @endpoint)
      |> EndpointPathNode.add_child(
        EndpointPathNode.new("child2", @endpoint)
        |> EndpointPathNode.add_child(EndpointPathNode.new("child3", @endpoint))
      )

    assert 3 == EndpointPathTree.height(root)
  end

  test "EndpointPathTree.find/2 returns error because path is longer than the tree height" do
    root = EndpointPathNode.new("api", @endpoint)
    assert {:error, :not_found} == EndpointPathTree.find(root, "/api/soendpoint")
  end

  test "EndpointPathTree.find/2 with 2 path parts returns matching endpoint" do
    node =
      EndpointPathNode.new("/", @endpoint)
      |> EndpointPathNode.add_child(
        EndpointPathNode.new("api", @endpoint)
        |> EndpointPathNode.add_child(EndpointPathNode.new("someendpoint", @endpoint))
      )

    assert {:ok, %EndpointPathNode{endpoint: @endpoint, children: %{}, path: "someendpoint"}} ==
             EndpointPathTree.find(node, "api/someendpoint")
  end

  test "EndpointPathTree.find/2 with 2 path parts returns error because path does not exist" do
    node =
      EndpointPathNode.new("api", @endpoint)
      |> EndpointPathNode.add_child(EndpointPathNode.new("someendpoint", @endpoint))

    assert {:error, :not_found} == EndpointPathTree.find(node, "/api/some")
  end

  test "EndpointPathTree.find/2 with 3 path parts returns matching endpoint" do
    node =
      EndpointPathNode.new("/", @endpoint)
      |> EndpointPathNode.add_child(
        EndpointPathNode.new("api", @endpoint)
        |> EndpointPathNode.add_child(
          EndpointPathNode.new("someendpoint", @endpoint)
          |> EndpointPathNode.add_child(
            EndpointPathNode.new("third_level", "http://localhost:8085")
          )
          |> EndpointPathNode.add_child(
            EndpointPathNode.new("third_level_other_endpoint", @endpoint)
          )
        )
      )

    assert {:ok,
            %EndpointPathNode{
              children: %{},
              path: "third_level",
              endpoint: "http://localhost:8085"
            }} ==
             EndpointPathTree.find(node, "api/someendpoint/third_level")
  end

  test "EndpointPathTree.find/2 with path param" do
    node =
      EndpointPathNode.new("/", @endpoint)
      |> EndpointPathNode.add_child(
        EndpointPathNode.new("api", @endpoint)
        |> EndpointPathNode.add_child(
          EndpointPathNode.new("users", @endpoint)
          |> EndpointPathNode.add_child(
            EndpointPathNode.new("{userId}", "http://localhost:8085")
            |> EndpointPathNode.add_child(EndpointPathNode.new("info", "http://localhost:8085"))
          )
        )
      )

    assert {:ok,
            %EndpointPathNode{children: %{}, path: "info", endpoint: "http://localhost:8085"}} ==
             EndpointPathTree.find(node, "api/users/1/info")
  end
end
